var gulp 		= require('gulp');
var browserSync = require('browser-sync').create();
var sass 		= require('gulp-sass');


gulp.task('serve', ['sass'], function(){
	
	browserSync.init({
		server: "./"
	});

	gulp.watch('assets/scss/*.scss', ['sass']);
	gulp.watch("*.html").on('change', browserSync.reload);
	gulp.watch("assets/scripts/*.js").on('change', browserSync.reload);

});

gulp.task('sass', function(){
	return gulp.src('assets/scss/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('assets/css'))
		.pipe(browserSync.stream());
});

gulp.task('default', ['serve'] );